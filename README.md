# Deluge AutoRemove Fix

Deluge docker seems not auto removing entries after exceeding seed ratio until you open the Web UI. This script simulates the opening of web UI to trigger the auto removal.