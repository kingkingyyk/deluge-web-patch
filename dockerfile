FROM python:3.10-alpine3.16

ENV URL=http://localhost:8112
ENV PASSWORD=deluge

ADD query.py requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt

CMD ["python", "query.py"]
