import os
import time
from requests import Session

url = f'{os.environ["URL"]}/json'
password = os.environ["PASSWORD"]

session = Session()
request_idx = 101
data = dict(id=request_idx, method="auth.login", params=[password])
response = session.post(url, json=data)
response.raise_for_status()
while True:
    data = dict(id=request_idx, method="web.update_ui", params=[["queue", "name"], {}])
    response = session.post(url, json=data)
    response.raise_for_status()
    time.sleep(10.0)
